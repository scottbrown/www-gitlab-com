---
layout: markdown_page
title: "Marketing"
---
## Marketing Handbook Index
* [Marketing Mission](#mission)
* [Functional Groups](#groups)
* [Marketing OKRs](#okrs)
* [Meetings and Structure](#meetings)
* [Marketing team SLAs](#sla)
* [Resources](#resources)

# GitLab Marketing Mission: GitLab for All!<a name="mission"></a>

We think GitLab (.com, CE, and EE) can help developers, designers, IT workers, marketers, and everyone in between improve collaboration.  

The GitLab marketing team is composed of a unique set of individuals from interesting backgrounds all over the world. Because of our unique skill sets, we're able to accomplish a lot of high impact campaigns through collaboration and open feedback from our team and the community.  

Here are some marketing resources we think help to describe how we work:   

- coming soon
- coming soon
- coming soon
- coming soon
- coming soon

# Marketing Team Functional Groups<a name="groups"></a>

Our Marketing team is split into four key functional groups. All four groups are equally important for the success of GitLab.

### Demand generation

- [Demand generation](/handbook/marketing/demand-generation)
- [Online marketing](/handbook/marketing/online-marketing/)
- [Business development](/jobs/business-development-representative/)

### Design

- [Designer](/jobs/designer/)

### Developer Relations

- [Technical writing](/jobs/technical-writer/)
- [Developer advocacy](/handbook/marketing/developer-relations/developer-advocacy/)
- [Field marketing](/handbook/marketing/developer-relations/field-marketing/)

### Product Marketing

- [Product marketing](/handbook/marketing/product-marketing/)
- [Partner marketing](/handbook/marketing/product-marketing/#partnermarketing/)
  [Content marketing](/handbook/marketing/developer-relations/content-marketing/)

# Marketing OKRs<a name="okrs"></a>

Our team and the demands on marketing are growing quickly. In order to align our goals with company goals as well as prioritize what we are working on, OKRs help us to maintain structure. Of course, not everything can be captured in one Google Sheet but this helps us all to know what we consider our goals as a team to be.  

Each member of the marketing team is responsible for 3 Objectives and 3 Key Results for each objective.  

To find the GitLab Marketing Team's OKRs, search the company Google Drive for "Marketing OKRs - Quarterly".

- What is an OKR? Objectives and Key Results
- Objectives are goals
- Key Results are measurement
- OKRS help to prioritize and align
- OKRs are set quarterly
- OKRs are NOT a measurement of performance.
- Set goals that aren’t easy to hit.
- Graded on a scale of 0.0 to 1.0. Normal is around .8
- [Quick overview of OKRs](http://www.slideshare.net/HenrikJanVanderPol/how-to-outperform-anyone-else-introduction-to-okr)

# Meetings and Structure<a name="meetings"></a>  

These are just the required meetings for team members and managers. Of course, meetings are encouraged when it expedites a project or problem solving amongst member so the team and company. Don't be afraid to say "Hey, can we hangout?" if you need help with something.

### Weekly 1:1 (New hire: First month with all direct reports)

**Meeting goal: For manager to help new team member onboard quickly.**  

**Run time: 30 minutes**

All managers should have a weekly 1:1 with their direct reports in the first month of employment, starting with the first day of employment where possible.  

The first meeting should run as follows:  

- Welcome and make sure new hire is working through onboarding tasks.  
- Suggest people on the marketing team and beyond to meet with.  
- Manager should make sure all technology needs are taken care of.  
- Manager should answer or help to find proper resource for any questions.  
- Manager should create Google Doc private 1:1 agenda for all recurring 1:1's and add to the description of the calendar invite.  

The agenda of the following 1:1s should be the same as the recurring Bi-weekly 1:1s with time set aside to answer any questions about onboarding.


### Bi-weekly 1:1 (After first month with all direct reports)

**Meeting goal: For manager to remove roadblocks and help prioritize so team member can be effective.**  

**Run time: 30 minutes**

All managers should have twice monthly (bi-weekly) meetings with all of his or her direct reports.

The meeting should run as follows:  

- Always add agenda items to Google Doc agenda in description of meeting invite.   
- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?    
- Go through the agenda and answer any questions.    
- Action items for the employee should be marked in Red.    
- Action items for the manager should be marked in Blue.    
- All action items should be handled before next 1:1 unless otherwise noted.    

### Every 6 weeks 1:1 (All members of marketing team with executive management)

**Meeting goal: For CMO to help with any questions and career path discussion.**  

**Run time: 30 minutes**

All members of the marketing team that are not direct reports should meet with their executive management (CMO) once every 6 weeks. If questions or concerns arise, please don't hesitate to reach out directly for an impromptu discuss via email or slack.

The meeting should run as follows:  

- First discuss any issues or problems that the manager can help with. Any roadblocks or disagreements?  
- Talk about career development and opportunities for growth.  
- Walk through quarterly OKRs to make sure they are relevant and see if help is needed.  
- Discuss upcoming OKRs if close to end of a quarter.  

### Bi-weekly Marketing Meeting

**Meeting goal: For the entire company to gain insight into what the Marketing team is focusing on.**

**Run time: 30 minutes**

The bi-weekly marketing meeting is open to the entire company for questions, ideas, and feedback. If you'd like to be added to this meeting, please contact anyone on the marketing team to be added to the invite.

To find the agenda for the bi-weekly marketing meeting, search the company Google Drive folder for "Bi-Weekly Marketing Meeting Agenda"

The meeting should run as follows:  

- 24 hours prior to the meeting all marketing members should have their sections of the agenda completed.  
- The CMO will email all attendees reminding them to take a look at the agenda and add any questions to the top of the meeting.  
- Meeting will open with the CMO giving an overview of new hires, organization changes, large upcoming campaigns, etc.  
- The meeting will then open to questions starting with the questions already documented.
- Everyone is encouraged to participate.    

### Tuesday & Thursday Dinosaur Party   

**Meeting goal: Dinosaur Party! (and also make sure we're all working together effectively)**    
**Run time: 10-15 minutes**  

The Tuesday and Thursday Dinosaur Party is where the entire team meets for 10-15 minutes immediately following the team call to discuss what we're all working on and to get help with any questions. There is no agenda for this meeting and it should be an open forum.  

The meeting should run as follows:  

- First 60 seconds: Post funny GIFs in RocketChat marketing room and person with funniest gets a dinosaur sticker. Person with most dinosaur stickers per quarter gets $250 bonus. Voting occurs with Smiley face emojis  
- First 5 minutes: Everyone in order of Team Page (Last goes first) gives 15 seconds about what they are working on. (Marcia, Colton, Amara, Ryan, Amanda, Braden, Mitchell, Ivan, Luke, Axil, Hank, Ashley, Emily)  
- Last 5 minutes: Discuss roadblocks and any collaboration needs  

To find the Dinosaur Sticker leaderboard, search the company Google Drive for "Dinosaurs are awesome"

### Monthly Marketing Team Meeting

**Meeting goal: OKR review and large campaign planning**

**Run time: 30 minutes**

The Monthly Marketing Team Meeting is for review of our team OKRs (which we set at the Quarterly Marketing Review) and to discuss any large campaigns that are happening in the upcoming month.

The agenda for the meeting can be found by searching the company Google Drive for "Monthly Marketing Meeting"

The meeting should run as follows:

- Agenda items should be addressed first  
- Discuss any large upcoming campaigns  
- Team OKRs are quickly reviewed and graded   

### Quarterly Marketing Review

**Meeting goal: How did the last quarter go? What should we do for the upcoming quarter?**

**Run time: 120 minutes**

The Quarterly Marketing Review takes the place of the Monthly Marketing Meeting every last month of the new Quarter.

CMO works with the functional group leads to review planning for each group.

The meeting should run as follows:

- CMO leads review of over marketing strategies of last quarter. What worked? What didn't work? What should we do more of?  
- Head of Product Marketing reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Developer Relations reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Demand Generation reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Head of Design reviews upcoming goals and plans for product marketing. Floor opens for discussion and brainstorming.  
- Team agrees on what key takeaways are from the last quarter.  
- Team agrees on what our Team OKRs should be for the upcoming quarter.  

# Marketing team SLAs (Service Level Agreements)<a name="sla"></a>

When working remotely in such a quick moving organization, it is important for a team to agree on a few basic service level agreements on how we would like to work together. With anything, things can come up that make it not possible to meet these SLAs but we all agree to use best effort when possible.    

- Respond to your emails by end of next business day.
- Respond when you are cc'd with an action item on issues by end of next business day.  
- Be on time to meetings. We start at on time.  
- Acknowledge receipt of emails (community@, FYIs) by BCC'ing the list.
- Try not to email co-workers on weekends. Try out [Boomerang](http://www.boomeranggmail.com/) and set all your emails to send Monday morning at 6 AM. People will think you're up and working early! Time off is important. We all have stressful weeks so please unplug on the weekends where possible.
- Do not ping someone in a public channel on RocketChat on the weekends. This is rude.

# Marketing resources<a name="resources"></a>

- [GitLab Marketing Project](https://gitlab.com/gitlab-com/marketing)
- [Public GitLab Marketing Issue Tracker](https://gitlab.com/gitlab-com/marketing/issues)
- Google drive folder can be found by searching the company Google Drive for
  "GitLab Marketing"
