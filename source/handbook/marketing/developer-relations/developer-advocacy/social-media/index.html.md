---
layout: markdown_page
title: "Developer Advocacy - Social Media"
---

## Social Media Guidelines

Company-wide guidelines for social media can be found [here](/handbook/social-media-guidelines).


## Social Media Channels

GitLab has a presence across many social media channels. The following is a list of channels we actively monitor.

* [The GitLab blog](#)
* [GitLab issue trackers](#)
* [Twitter](#)
* [Facebook](#)
* [LinkedIn](#)
* [Hacker News](#)
* [Quora](#)
* [Reddit](#)
* [IRC](#)
* [Stack Overflow](#)
* [Slack](#)

## Twitter

There are two GitLab Twitter accounts

-   [@GitLab](https://twitter.com/gitlab) -  This is the main account for the project and company, which is managed by team members at GitLab, Inc. Everything tweeted, RTd, or liked from @GitLab also gets promoted to http://about.gitlab.com

-   [@GitLabStatus](https://twitter.com/gitlabstatus) - This account tweets about the status of GitLab.com services and is managed by the GitLab infrastructure team.
We don't generally retweet GitLabStatus, but point users to follow that account or check it.

### Representing GitLab on Twitter

Express gratitude

-   Favorite tweets which include positive comments about GitLab or articles mentioning GitLab in a positive way.

-   Thank users for feedback and comments with @mentions like: ‘Thank you’, ‘Glad to hear that’, ‘You’re welcome’, do this even when you already favored something.

Be friendly

-   When responding to tweets, be polite and brief.

-   It’s OK to invite CE or GitLab.com users to collaborate, the easiest way is to direct a user to the relevant issue tracker.

About using the tools

-   Zendesk is a good place to reply to tweets, but always check for the tweet history on Tweetdeck.

-   Use Tweetdeck to find GitLab mentions without the # or @.

Use English

- [GitLab communicates in English](https://about.gitlab.com/handbook/#internal-communication) so please tweet in English.

- If a foreign language tweet comes in, replies in that language are okay but discouraged as we can't guarantee making our support SLA.

- Please no foreign retweets, these break the flow of people reading their timeline and cause people to unfollow.

### Replies to commonly asked questions on Twitter

-   Feature proposals: It would be really cool if GitLab could… 

    -   If this is a first time feature request: Ask them to submit a feature request on the issue tracker for [GitLab CE](https://gitlab.com/gitlab-org/gitlab-ce/issues) or [GitLab EE](https://gitlab.com/gitlab-org/gitlab-ee/issues) and add the label <code>feature proposal</code>

    -   If there’s an existing feature request, ask the user to vote on the request with a link to the feature request.

    -   If we’re accepting merge requests, tell them with a link to the feature request. Often the feature request thread will indicate that as well.

-   Bug reports: I think I’ve discovered a bug. 

    -   Bug reports are welcome. Direct them to this guide on reporting a bug. <https://gitlab.com/gitlab-org/gitlab-ce/blob/master/CONTRIBUTING.md>

-   Product specific questions: I need some help with X.

    -   Check if there is existing documentation and reply with a link if there is.

    -   GitLab.com specific: Ask the user to add the Issue to our GitLab.com Support Forum. https://gitlab.com/gitlab-com/support-forum/issues

    -   GitLab CE or CI: ask the user to add the Issue to our CE Issue tracker. https://gitlab.com/gitlab-org/gitlab-ce/issues

-   Support or help: I think I ran into a problem.

    -   Respond with a quote and CC [@GitLabSupport](http://twitter.com/gitlabsupport). Using a quote means pasting the full URL to the author’s original tweet into your reply. Check to make sure you are also replying directly to their account and not yourself.

-   GitLab is down!

    -   If there is a known issue, apologize and invite them to follow [@GitLabStatus](https://twitter.com/gitlabstatus)

    -   If this is not known, alert the infrastructure team, and thank the reporter. 

-   Request for consulting or development.

    - If a GitLab user would like to engage the GitLab team for custom consulting, for example to sponsor a feature, they can contact via this form: [Development](https://about.gitlab.com/development/).

## Facebook

- We post blog articles there if we think they are good and relevant.
- Share "Inside GitLab" stories which highlight the people behind the product.

## YouTube

- We post our webcast recordings there.

## LinkedIn

- We post information for potential GitLab employees to show what it's like to work here.
- We share jobs
- We post links to relevant industry articles about open source, DevOps, or other trends and news.

## Other channels

See the ones listed on our [getting help page](https://about.gitlab.com/getting-help/).
