---
layout: markdown_page
title: "Developer Advocacy"
---

Go to the [Marketing Handbook](/handbook/marketing)

Developer Advocates act as spokespeople and liaisons between GitLab and its users. Their role is to make developers happy to use GitLab by getting customer feedback into the product.

Advocates facilitate communication about GitLab via:

- Code samples/snippets
- Public speaking and events
- Social media
- Blogging and podcasting
- Training programs
- Customer support

## Code

In addition to providing enhancements to GitLab itself, Advocates can also provide miscellaneous code samples for everyone to view. (Specific examples TBD).

## Public Speaking and Events

Speaking at events is an important facet of developer outreach. Guidelines for what to do at events can be found [here](/handbook/marketing/developer-relations/developer-advocacy/events), while guidelines for public speaking can be found [here](/handbook/marketing/developer-relations/developer-advocacy/events/public-speaking.html).

## Social Media and online community interaction

Advocates follow these [guidelines](/handbook/social-media-guidelines) when posting as GitLab. 

## Blogging and Podcasting

The developer relations team may need technical content that is more abstract than how-to technical guides. Developer Advocates can produce relevant content to fill the content calendar.

## Training Programs

TBD.

## Customer Support

While they're not full-time support team members, advocates can field some of the questions the support team recieves.