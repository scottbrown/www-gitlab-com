---
layout: markdown_page
title: "Developer Advocacy - Events"
---

Events are an important part of developer relations efforts. You can expect to travel a lot. Keep track of which events you go to via your calendar and make sure you take some vacation time.

## Selecting Events

TBD.

## Before the Event

Be sure to use social channels to let people know you'll be there. If you're giving a talk, make sure to include that too. Add events to your calendar and make sure to let everyone else on the developer relations team know so they can plan accordingly.

## Travel

- Book flights that cause the least amount of agony without being over the top. A 12 hour layover to save $200 does no one any favors.
- Try to arrive the day before the event begins, and try to leave the day after. 
- Stay at the conference hotel if possible. Attendees tend to hang out there.

## At The Event

- Attend the event and be present even if you're done speaking.
- Go to talks by other speakers and tweet things you find interesting.
- Use the official event hashtag (if available) to find other attendees on Twitter.
- Retweet conference updates put out by the organizers.
- Be the first in and the last out. When attendees are starting their day or headed to an after-party, you should make an appearance.
- Get a good night's sleep the night before any presentations you give.
- If you're giving a talk, use the official hashtag to share location/time as well as your slides once you're done.

## After the Event

- Follow up with contacts you met/anyone who handed you a business card. Even if it's just to say "Thanks for stopping by!"
- Fill out an event [debrief](event-debrief.html)
- Relax!

