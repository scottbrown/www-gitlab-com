---
layout: markdown_page
title: "Developer Advocacy - Event Debrief"
---

## Event Debrief

After an event, put a debrief together to help us determine whether we're hitting our goals. A short summary should answer:

- Overall feedback about the event. Was the location okay? Was it hard to get to? Was it well organized?
- Overview of who was there (Developers? C-levels? Non-technical people?)
- Were there any competitors there? What was their presence like?
- Who did you speak to that we need to follow up with? Did you meet any customer/hiring prospects?
- What went well?
- What didn't go so well?
- What can we improve?
- Would you attend/sponsor/speak at this event again?

This information can be posted on the relevant event issue in the issue tracker. If no issue exists, please create one and tag Emily in it. If you gave a talk be sure to include a [debrief](#talk-debrief) about that as well.

Make sure to follow up with anyone you spoke to at the event, even if it's just to say "Thanks for chatting!"

Event organizers also love to hear feedback about their events. Make sure to thank them and tell them what they did well. If things didn't go according to plan, make sure they know that too so they can incorporate your feedback next time.

## Talk Debrief

After speaking, it's important to make note of things that went well and things that need improvement. You can use the following template to get started (but feel free to modify it). Put your debrief in a comment on the event's issue. If no issue exists, please create it and tag Emily so she sees.

```
Event Name:
Event Dates:
Event Location:

Talk Title:
Talk Presenter(s):
Talk Slides (if available online somewhere):
Talk Abstract:
Talk Time (include timezone):
Attendee Estimate (how many people attended your session?):

How was the talk received by the audience?

What went well?

What didn't go so well?

Were there any technical difficulties?

What questions did people ask?
```